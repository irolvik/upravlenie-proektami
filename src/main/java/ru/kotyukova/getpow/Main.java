package ru.kotyukova.getpow;

import java.util.Scanner;

class Main
{
    public static double getPow(double digit, int pow)
    {
	if(pow == 0)	// любое число в степени 0 равно 1
	{ return 1.0; }
	
	else if(pow < 0)
	{ return 1.0 / getPow(digit, -pow); } // в случае отрицательной степени используем формулу a^-n = 1 / a^n

	else if(pow % 2 == 0)
	{
	    double result = getPow(digit, pow / 2);    // если стпень четная, вызываем функцию для pow / 2 
	    return result * result;    // и вернем квадрат результата
	}

	else
	{ return digit * getPow(digit, pow - 1); } // усли стпень нечетная, вернем результат исходя из a^n = a * a^(n-1)
    }

    public static void main(String[] args)
    {
	double digit 	= 0.0;
	int pow 	= 0; 
	Scanner scanner = new Scanner(System.in);

	System.out.println("Расчитываем степень (n) числа (a).");

	System.out.print("Число a: ");
	digit = scanner.nextDouble();

	System.out.print("Степень n: ");
	pow = scanner.nextInt();

	System.out.println("Результат: " + getPow(digit, pow));
    }
}
