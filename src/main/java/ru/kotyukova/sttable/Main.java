package ru.kotyukova.sttable;

import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

class Main
{
    public static void main(String[] args)
    {
	List<Student> studentList = new ArrayList();
	Scanner scanner = new Scanner(System.in);


	int ch = 0;
	while(ch != -1)
	{
	    System.out.println("1. Вывыод информации");
	    System.out.println("2. Ввод информации");
	    System.out.println("0. Выход");

	    ch = scanner.nextInt();

	    switch(ch)
	    {
		case 1:
		    {
			if(studentList.size() == 0)
			{ System.out.println("Список пуст!"); break;}

			//System.out.print("ФИО: ");
			//System.out.print("\t\tДата рождения: \n");

			System.out.println("\n\n");
			String[] colNameStudentArr = {"ФИО", "Дата рождения"};
			String studentFormat = "%-40s%-40s\n";

			System.out.format(studentFormat, (Object[]) colNameStudentArr);
			System.out.println("------------------------------------------------------------------------------------");

			for(Student student : studentList)
			{
			    String[] rowDataStudent = {student.getFIO(), student.getBornDate()};
			    System.out.format(studentFormat, (Object[]) rowDataStudent);

			    System.out.println("\n");
			    String[] colNameRecordBookArr = {"Предмет", "Фио преаодавателя", "Дата экзамена"};
			    String recordBookFormat = "%-35s%-35s%-35s\n";

			    System.out.format(recordBookFormat, (Object[]) colNameRecordBookArr);
			    System.out.println("------------------------------------------------------------------------------------");

			    for(Subject subject : student.getRecordBook().getSubjectList())
			    {
				String[] rowDataRecordBook = {subject.getSubjectName(), subject.getTeacherFIO(), subject.getExamDate()};
				System.out.format(recordBookFormat, (Object[]) rowDataRecordBook);
			    }

			    System.out.println("\n\n");
			}

			break;
		    }
		case 2:
		    {
			Scanner inputDataScanner = new Scanner(System.in);
			
			System.out.print("\n\nФИО студента: ");
			String studentFIO = inputDataScanner.nextLine();
			System.out.print("\nДата рождения студента: ");
			String studentBornDate = inputDataScanner.nextLine();
			System.out.println("\n\n");

			studentList.add(new Student(studentFIO, studentBornDate));

			break;
		    }
		case 0:
		    { System.out.println("Выход"); ch = -1; break; }
		default:
		    { System.out.println("Выход"); ch = -1; break; }
	    }
	}
    }
}
