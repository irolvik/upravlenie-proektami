package ru.kotyukova.sttable;

import java.util.List;
import java.util.ArrayList;

public class RecordBook
{
    List<Subject> subjectList;

    public RecordBook()
    {
	subjectList = new ArrayList();
	subjectList.add(new Subject("OSINT", "Преподаватель 1", "01.09.2023"));
	subjectList.add(new Subject("IT право", "Преподаватель 2", "02.09.2023"));
	subjectList.add(new Subject("Технология личностного роста", "Преподаватель 3", "03.09.2023"));
	subjectList.add(new Subject("Технология МО в кибербезопасности", "Преподаватель 4", "04.09.2023"));
    }

    // --- GET
    public List<Subject> getSubjectList()
    { return this.subjectList; }
    //
}
