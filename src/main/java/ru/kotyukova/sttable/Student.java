package ru.kotyukova.sttable;


class Student
{
    String FIO;
    String bornDate;
    RecordBook recordBook;

    public Student(String FIO, String bornDate)
    {
	this.FIO	= FIO;
	this.bornDate	= bornDate;
	this.recordBook	= new RecordBook();
    }

    // --- GET
    public String getFIO()
    { return this.FIO; }

    public String getBornDate()
    { return this.bornDate; }

    public RecordBook getRecordBook()
    { return this.recordBook; }
    //
}
