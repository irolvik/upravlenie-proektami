package ru.kotyukova.sttable;

public class Subject
{
    String subjectName;
    String teacherFIO;
    String examDate;

    public Subject(String subjectName, String teacherFIO, String examDate)
    {
	this.subjectName	= subjectName;
	this.teacherFIO		= teacherFIO;
	this.examDate		= examDate;
    }

    // --- GET
    public String getSubjectName()
    { return this.subjectName; }
    
    public String getTeacherFIO()
    { return this.teacherFIO; }

    public String getExamDate()
    { return this.examDate; }
    //
}
